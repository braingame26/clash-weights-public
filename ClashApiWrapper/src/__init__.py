from flask import Flask
import requests


app = Flask(__name__)

token = '<INSERT_TOKEN>'

@app.route('/coc/v1/clans/<clanTag>/members')
def clanMembers(clanTag):
    
    clanTag = clanTag.replace("#", "%23")
    
    endpoint = "https://api.clashofclans.com/v1/clans/" + clanTag + "/members?limit=50"
     
    headers = {"Authorization":"Bearer " + token}
    
    response = requests.get(endpoint, headers=headers).json()
    
    return str(response)

@app.route('/coc/v1/players/<playerTag>')
def player(playerTag):
    
    playerTag = playerTag.replace("#", "%23")
    
    endpoint = "https://api.clashofclans.com/v1/players/" + playerTag
     
    headers = {"Authorization":"Bearer " + token}
    
    response = requests.get(endpoint, headers=headers).json()
    
    return str(response)

@app.route('/coc/v1/clans/<clanTag>')
def clan(clanTag):
    
    clanTag = clanTag.replace("#", "%23")
    
    endpoint = "https://api.clashofclans.com/v1/clans/" + clanTag
     
    headers = {"Authorization":"Bearer " + token}
    
    response = requests.get(endpoint, headers=headers).json()
    
    return str(response)

if __name__ == "__main__":
    app.run()