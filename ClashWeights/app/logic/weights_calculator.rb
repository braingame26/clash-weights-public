require 'cgi'
require 'net/http'
require 'yaml'
require 'player_weights'

class WeightsCalculator
  
  @@weights = 
  {
    "barbarian" => [ 100, 200, 300, 390, 470, 541, 601 ],
    "archer" => [ 150, 300, 450, 590, 720, 840, 951 ],
    "giant" => [ 120, 240, 360, 510, 685, 886, 111, 1362 ],
    "goblin" => [ 50, 100, 150, 195, 239, 283, 325 ],
    "wall breaker" => [ 100, 200, 300, 400, 500, 600, 700 ],
    "balloon" => [ 100, 220, 360, 520, 700, 900, 1120 ],
    "wizard" => [ 150, 300, 450, 599, 747, 895, 1041, 1187 ],
    "healer" => [ 180, 370, 570, 791, 1031 ],
    "dragon" => [ 170, 340, 510, 785, 1235, 1715 ],
    "p.e.k.k.a" => [ 120, 270, 450, 661, 1011, 1360 ],
    "baby dragon" => [ 200, 450, 700, 950, 1201 ],
    "miner" => [ 225, 525, 900, 1350, 1876 ],
    "minion" => [ 120, 240, 360, 475, 585, 690, 791 ],
    "hog rider" => [ 120, 250, 390, 540, 740, 990, 1291 ],
    "valkyrie" => [ 100, 230, 390, 580, 801, 1051 ],
    "golem" => [ 170, 340, 560, 830, 1230, 1730, 2331 ],
    "witch" => [ 1000, 1750, 2250 ],
    "lava hound" => [ 1000, 1750, 2250, 2500 ],
    "bowler" => [ 1400, 3000, 4800 ],
    "barbarian king" => [ 40.0, 80.0, 120.0, 160.0, 200.0, 240.0, 280.0, 320.0, 360.0, 400.0, 440.0, 480.0, 520.0, 560.0, 600.0, 640.0, 680.0, 720.0, 760.0, 780.0, 800.0, 820.0, 840.0, 860.0, 880.0, 900.0, 920.0, 940.0, 960.0, 980.0, 1000.0, 1020.0, 1040.0, 1060.0, 1080.0, 1100.0, 1120.0, 1140.0, 1160.0, 1180.0, 1200.0, 1220.0, 1240.0, 1260.0, 1280.0, 1295.0, 1310.0, 1325.0, 1340.0, 1355.0],
    "archer queen" => [ 50.0, 100.0, 150.0, 200.0, 250.0, 300.0, 350.0, 400.0, 450.0, 500.0, 550.0, 600.0, 650.0, 700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 985.0, 1020.0, 1055.0, 1090.0, 1125.0, 1160.0, 1195.0, 1230.0, 1265.0, 1300.0, 1335.0, 1370.0, 1405.0, 1440.0, 1475.0, 1510.0, 1545.0, 1580.0, 1615.0, 1650.0, 1685.0, 1720.0, 1755.0, 1790.0, 1825.0, 1860.0, 1880.0, 1900.0, 1920.0, 1940.0, 1960.0],
    "grand warden" => [100.0, 200.0, 300.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1100.0, 1200.0, 1300.0, 1400.0, 1500.0, 1600.0, 1700.0, 1800.0, 1900.0, 2000.0],
    "lightning spell" => [ 60.0, 125.1, 195.3, 270.6, 351.0, 436.5, 527.1 ],
    "healing spell" => [ 230.0, 445.1, 645.3, 830.6, 1001, 1156.5, 1297.1 ],
    "rage spell" => [ 100.0, 195.1, 285.3, 370.6, 451.0 ],
    "jump spell" => [ 200, 700, 800 ],
    "freeze spell" => [ 1000.0, 1999.1, 2997.3, 3994.6, 4991.0, 5986.5 ],
    "clone spell" => [ 300.0, 630.1, 990.3, 1380.6, 1801.0 ],
    "poison spell" => [ 160.0, 335.1, 525.3, 730.6, 951.0 ],
    "earthquake spell" => [ 150.0, 295.1, 435.3, 570.6 ],
    "haste spell" => [ 240.0, 480.1, 720.3, 960.6 ],
    "skeleton spell" => [ 175.0, 365.1, 570.3, 785.6 ]
  }
  
  def calculate_clan_weights(cTag)
    
    cWeights = ClanWeights.new
    cWeights.tag = cTag
    cTag = CGI.escape(cTag)
    url = "http://clashweights.com/api/coc/v1/clans/" + cTag
    uri = URI(url)
    response = Net::HTTP.get(uri)
    hash = YAML.load(response)
    if hash["name"] == nil
      return nil
    end
    cWeights.name = hash["name"]
    memberList = hash["memberList"]
    
    members = Array.new
    oWeight = 0
    for member in memberList
      pTag = CGI.escape(member["tag"])
      pWeights = calculate_player_weights(pTag)
      members.push(pWeights)
      oWeight = oWeight + pWeights.oWeight
    end
    cWeights.memberList = members.sort_by { |p| p.oWeight }.reverse!
    cWeights.oWeight = oWeight.round
    return cWeights
    
  end
  
  def calculate_player_weights(pTag)
    hash = call_api(pTag)
    if hash["name"] == nil
      return nil
    end
    pWeights = PWeights.new
    pWeights.tag = pTag
    pWeights.name = hash["name"]
    get_spells(hash, pWeights)
    get_troops(hash, pWeights)
    get_heroes(hash, pWeights)
    total_offensive_weight(pWeights)
    return pWeights
  end
  
  private
  def call_api(pTag)
    pTag = CGI.escape(pTag)
    url = "http://clashweights.com/api/coc/v1/players/" + pTag
    uri = URI(url)
    response = Net::HTTP.get(uri)
    yaml = YAML.load(response)
    return yaml
  end
  
  private
  def get_spells(hash, pWeights)
    spells = hash["spells"]
    pWeights.lightningWeight = get_item_weight(spells, "lightning spell")
    pWeights.healWeight = get_item_weight(spells, "healing spell")
    pWeights.rageWeight = get_item_weight(spells, "rage spell")
    pWeights.jumpWeight = get_item_weight(spells, "jump spell")
    pWeights.freezeWeight = get_item_weight(spells, "freeze spell")
    pWeights.cloneWeight = get_item_weight(spells, "clone spell")
    pWeights.poisonWeight = get_item_weight(spells, "poison spell")
    pWeights.eqWeight = get_item_weight(spells, "earthquake spell")
    pWeights.hasteWeight = get_item_weight(spells, "haste spell")
    pWeights.skeletonWeight = get_item_weight(spells, "skeleton spell")
  end
  
  private
  def get_troops(hash, pWeights)
    troops = hash["troops"]
    pWeights.barbWeight = get_item_weight(troops, "barbarian")
    pWeights.archerWeight = get_item_weight(troops, "archer")
    pWeights.giantWeight = get_item_weight(troops, "giant")
    pWeights.goblinWeight = get_item_weight(troops, "goblin")
    pWeights.wBreakerWeight = get_item_weight(troops, "wall breaker")
    pWeights.balloonWeight = get_item_weight(troops, "balloon")
    pWeights.wizardWeight = get_item_weight(troops, "wizard")
    pWeights.healerWeight = get_item_weight(troops, "healer")
    pWeights.dragonWeight = get_item_weight(troops, "dragon")
    pWeights.pekkaWeight = get_item_weight(troops, "p.e.k.k.a")
    pWeights.bDragonWeight = get_item_weight(troops, "baby dragon")
    pWeights.minerWeight = get_item_weight(troops, "miner")
    pWeights.minionWeight = get_item_weight(troops, "minion")
    pWeights.hogWeight = get_item_weight(troops, "hog rider")
    pWeights.valkWeight = get_item_weight(troops, "valkyrie")
    pWeights.golemWeight = get_item_weight(troops, "golem")
    pWeights.witchWeight = get_item_weight(troops, "witch")
    pWeights.houndWeight = get_item_weight(troops, "lava hound")
    pWeights.bowlerWeight = get_item_weight(troops, "bowler")
  end
  
  private
  def get_heroes(hash, pWeights)
    heroes = hash["heroes"]
    pWeights.bkWeight = get_item_weight(heroes, "barbarian king")
    pWeights.aqWeight = get_item_weight(heroes, "archer queen")
    pWeights.gwWeight = get_item_weight(heroes, "grand warden")
  end
  
  private
  def total_offensive_weight(pWeights)
    pWeights.oWeight = pWeights.barbWeight +
    pWeights.archerWeight +
    pWeights.giantWeight +
    pWeights.goblinWeight +
    pWeights.wBreakerWeight +
    pWeights.balloonWeight +
    pWeights.wizardWeight +
    pWeights.healWeight +
    pWeights.dragonWeight +
    pWeights.pekkaWeight +
    pWeights.bDragonWeight +
    pWeights.minerWeight +
    pWeights.minionWeight +
    pWeights.hogWeight +
    pWeights.valkWeight +
    pWeights.golemWeight +
    pWeights.witchWeight +
    pWeights.houndWeight +
    pWeights.bowlerWeight +
    pWeights.lightningWeight +
    pWeights.healWeight +
    pWeights.rageWeight + 
    pWeights.jumpWeight +
    pWeights.freezeWeight +
    pWeights.cloneWeight +
    pWeights.poisonWeight +
    pWeights.eqWeight +
    pWeights.hasteWeight +
    pWeights.skeletonWeight +
    pWeights.aqWeight +
    pWeights.bkWeight +
    pWeights.gwWeight
  end
  
  private
  def get_item_weight(items, name)
    value = items.detect {|item| item["name"].downcase == name.downcase }
    if value != nil
      level = value["level"]
      begin
        return @@weights[name][level - 1].round
      rescue
        return 0
      end
    end
    return 0
  end
end