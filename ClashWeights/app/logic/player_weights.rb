class PWeights
  
  attr_accessor :name
  attr_accessor :tag
  
  attr_accessor :oWeight
  attr_accessor :barbWeight
  attr_accessor :archerWeight
  attr_accessor :giantWeight
  attr_accessor :goblinWeight
  attr_accessor :wBreakerWeight
  attr_accessor :balloonWeight
  attr_accessor :wizardWeight
  attr_accessor :healerWeight
  attr_accessor :dragonWeight
  attr_accessor :pekkaWeight
  attr_accessor :bDragonWeight
  attr_accessor :minerWeight
  attr_accessor :minionWeight
  attr_accessor :hogWeight
  attr_accessor :valkWeight
  attr_accessor :golemWeight
  attr_accessor :witchWeight
  attr_accessor :houndWeight
  attr_accessor :bowlerWeight
  attr_accessor :lightningWeight
  attr_accessor :healWeight
  attr_accessor :rageWeight
  attr_accessor :jumpWeight
  attr_accessor :freezeWeight
  attr_accessor :cloneWeight
  attr_accessor :poisonWeight
  attr_accessor :eqWeight
  attr_accessor :hasteWeight
  attr_accessor :skeletonWeight
  attr_accessor :bkWeight
  attr_accessor :aqWeight
  attr_accessor :gwWeight
  
end