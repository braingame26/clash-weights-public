require 'cgi'

class WelcomeController < ApplicationController
  
  def submit
    if params[:pTag].present?
      redirect_to "/player/index?pTag=" + CGI.escape(params[:pTag])
    elsif params[:cTag].present?
      redirect_to "/clan/index?cTag=" + CGI.escape(params[:cTag])
    end
  end
  
end