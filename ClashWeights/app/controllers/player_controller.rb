class PlayerController < ApplicationController
  
  def index
    pTag = params[:pTag]
    calc = WeightsCalculator.new
    @pWeight = calc.calculate_player_weights(pTag)
    if @pWeight == nil
      @errors = "Could not find information for player tag: " + pTag
    end
  end
  
end
