class ClanController < ApplicationController
  
  def index
    cTag = params[:cTag]
    calc = WeightsCalculator.new
    @cWeights = calc.calculate_clan_weights(cTag)
    if @cWeights == nil
      @errors = "Could not find information for clan tag: " + cTag
    end
    
  end
end
