Rails.application.routes.draw do
  get 'contact/index'

  get 'about/index'

  get 'clan/index'

  get 'player/index'

  post 'welcome/index' => "welcome#submit"

  root 'welcome#index'
end
