package com.evansalex.clashWarWeightsCmdLine;

import java.util.HashMap;
import java.util.Map;

public class WarWeights
{
	public static final Map<String, double[]> WAR_WEIGHTS = initWarWeights();

	private static Map<String, double[]> initWarWeights()
	{
		Map<String, double[]> weights = new HashMap<>();

		weights.put("Barbarian", new double[] { 100, 200, 300, 390, 470, 541, 601 });
		weights.put("Archer", new double[] { 150, 300, 450, 590, 720, 840, 950 });
		weights.put("Giant", new double[] { 120, 240, 360, 510, 685, 886, 111, 1362 });
		weights.put("Goblin", new double[] { 50, 100, 150, 195, 239, 283, 325 });
		weights.put("Wall Breaker", new double[] { 100, 200, 300, 399, 497, 595 });
		weights.put("Balloon", new double[] { 100, 200, 300, 420, 560, 760, 1021 });
		weights.put("Wizard", new double[] { 150, 300, 450, 599, 747, 895, 1041 });
		weights.put("Healer", new double[] { 180, 370, 570, 791 });
		weights.put("Dragon", new double[] { 170, 340, 510, 785, 1235, 1715 });
		weights.put("P.E.K.K.A", new double[] { 120, 270, 450, 661, 1011 });
		weights.put("Baby Dragon", new double[] { 150, 350, 600, 900, 1251 });
		weights.put("Miner", new double[] { 225, 525, 900, 1350 });
		weights.put("Minion", new double[] { 120, 240, 360, 475, 585, 690, 791 });
		weights.put("Hog Rider", new double[] { 120, 250, 390, 540, 740, 990, 1291 });
		weights.put("Valkyrie", new double[] { 100, 230, 390, 580, 791 });
		weights.put("Golem", new double[] { 170, 340, 560, 830, 1230, 1730 });
		weights.put("Witch", new double[] { 1000, 2000, 3000 });
		weights.put("Lava Hound", new double[] { 160, 400, 750, 1170 });
		weights.put("Bowler", new double[] { 400, 1000, 1800 });
		weights.put("Barbarian King", new double[] { 20.0, 39.0, 57.9, 76.7, 95.4, 114.0, 132.5, 150.9, 169.2, 187.4, 205.5, 223.5, 241.4,
						259.2, 276.9, 294.5, 312.0, 329.4, 346.7, 363.9, 381.0, 398.0, 414.9, 431.7, 448.4, 465.0,
						481.5, 497.9, 514.2, 530.4, 546.5, 562.5, 578.4, 594.2, 609.9, 625.5, 641.0, 656.4, 671.7,
						686.9, 702.0, 717.0, 731.9, 746.7, 761.4 });
		weights.put("Archer Queen", new double[] { 44.0, 87.0, 129.9, 172.7, 215.4, 258.0, 300.5, 342.9, 385.2, 427.4, 469.5, 511.5, 553.4,
						595.2, 636.9, 678.5, 720.0, 761.4, 802.7, 843.9, 885.0, 926.0, 966.9, 1007.7, 1048.4, 1089.0,
						1129.5, 1169.9, 1210.2, 1250.4, 1290.5, 1330.5, 1370.4, 1410.2, 1449.9, 1489.5, 1529.0, 1568.4,
						1607.7, 1646.9, 1686.0, 1725.0, 1763.9, 1802.7, 1841.4 });
		weights.put("Grand Warden", new double[] { 64.0, 127.0, 189.9, 252.7, 315.4, 378.0, 440.5, 502.9, 565.2, 627.4,
				689.5, 751.5, 813.4, 875.2, 936.9, 998.5, 1060.0, 1121.4, 1182.7, 1243.9 });
		weights.put("Lightning Spell", new double[] { 60.0, 125.1, 195.3, 270.6, 351.0, 436.5, 527.1 });
		weights.put("Healing Spell", new double[] { 230.0, 445.1, 645.3, 830.6, 1016.0, 1201.5 });
		weights.put("Rage Spell", new double[] { 100.0, 195.1, 285.3, 370.6, 451.0 });
		weights.put("Jump Spell", new double[] { 250.0, 450.1, 700.3 });
		weights.put("Freeze Spell", new double[] { 1000.0, 1999.1, 2997.3, 3994.6, 4991.0 });
		weights.put("Clone Spell", new double[] { 300.0, 630.1, 990.3, 1380.6 });
		weights.put("Poison Spell", new double[] { 160.0, 335.1, 525.3, 730.6, 951.0 });
		weights.put("Earthquake Spell", new double[] { 150.0, 295.1, 435.3, 570.6 });
		weights.put("Haste Spell", new double[] { 140.0, 280.1, 420.3, 560.6 });
		weights.put("Skeleton Spell", new double[] { 175.0, 365.1, 570.3, 785.6 });

		return weights;
	}
}
