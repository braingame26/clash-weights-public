package com.evansalex.clashWarWeightsCmdLine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.evansalex.clashWarWeightsCmdLine.output.ClanOutput;
import com.evansalex.clashWarWeightsCmdLine.pojo.Clan;
import com.evansalex.clashWarWeightsCmdLine.pojo.ClanWeight;

public class Run
{
	
	public void RunCmdLine()
	{
		ClashApi api = new ClashApi();
		
		while(true)
		{
			System.out.print("Please enter clan tag: ");
			String clanTag = readLine();
			if(clanTag == null)
			{
				System.out.println("Could not read clan tag");
				if(exit())
				{
					return;
				}
				else
				{
					continue;
				}
			}
			
			Clan clan = api.getClan(clanTag);
			if(clan == null)
			{
				System.out.println("Could not find clan");
				if(exit())
				{
					return;
				}
				else
				{
					continue;
				}
			}
			
			ClanWeight clanWeight = WarWeightCalculator.CalculateWeight(clan);
			if(clanWeight == null)
			{
				System.out.println("Could not calculate clan weight");
				if(exit())
				{
					return;
				}
				else
				{
					continue;
				}
			}
			
			System.out.println(ClanOutput.Output(clanWeight));
			if(exit())
			{
				return;
			}
		}
	}

	private String readLine()
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String line = null;
		try
		{
			line = br.readLine();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return line;
	}
	
	private boolean exit()
	{
		System.out.println("Exit (y,n)?");
		String exit = readLine();
		if(exit.equals("y"))
		{
			return true;
		}
		return false;
	}
	
	public static void main(String[] args)
	{
		new Run().RunCmdLine();
	}

}
