package com.evansalex.clashWarWeightsCmdLine.pojo;

public class PlayerProfile
{
	public String name;
	public String tag;
	public Troop[] troops;
	public Troop[] heroes;
	public Troop[] spells;
}
