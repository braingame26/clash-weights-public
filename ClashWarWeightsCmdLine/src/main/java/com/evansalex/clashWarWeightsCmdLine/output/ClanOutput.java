package com.evansalex.clashWarWeightsCmdLine.output;

import com.evansalex.clashWarWeightsCmdLine.pojo.ClanWeight;
import com.evansalex.clashWarWeightsCmdLine.pojo.PlayerWeight;

public class ClanOutput
{
	public static String Output(ClanWeight clanWeight)
	{
		StringBuilder str = new StringBuilder();
		
		appendLine(str, "Name: " + clanWeight.name);
		appendLine(str, "Tag: " + clanWeight.tag);
		str.append(System.lineSeparator());
		
		appendLine(str, "Member List");
		str.append(System.lineSeparator());
		for(PlayerWeight member : clanWeight.playerWeights)
		{
			appendLine(str, "Name: " + member.name);
			appendLine(str, "Tag: " + member.tag);
			appendLine(str, "Offensive Weight: " + member.offensiveWeight);
			str.append(System.lineSeparator());
		}
		
		return str.toString();
	}
	
	private static void appendLine(StringBuilder builder, String str)
	{
		builder.append(str).append(System.lineSeparator());
	}
}
