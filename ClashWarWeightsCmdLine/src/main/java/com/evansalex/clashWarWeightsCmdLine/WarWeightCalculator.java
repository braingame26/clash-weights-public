package com.evansalex.clashWarWeightsCmdLine;

import java.util.ArrayList;
import java.util.Arrays;

import com.evansalex.clashWarWeightsCmdLine.pojo.Clan;
import com.evansalex.clashWarWeightsCmdLine.pojo.ClanMember;
import com.evansalex.clashWarWeightsCmdLine.pojo.ClanWeight;
import com.evansalex.clashWarWeightsCmdLine.pojo.PlayerProfile;
import com.evansalex.clashWarWeightsCmdLine.pojo.PlayerWeight;
import com.evansalex.clashWarWeightsCmdLine.pojo.Troop;

public class WarWeightCalculator
{
	public static ClanWeight CalculateWeight(Clan clan)
	{
		ClashApi api = new ClashApi();
		PlayerWeight[] weights = new PlayerWeight[clan.memberList.length];
		double clanOffensiveWeight = 0;
		for(int i = 0; i < weights.length; i++)
		{
			ClanMember member = clan.memberList[i];
			PlayerProfile player = api.getPlayerProfile(member.tag);
			PlayerWeight weight = new PlayerWeight();
			weight.name = member.name;
			weight.tag = member.tag;
			weight.offensiveWeight = getPlayerOffensiveWeight(player);
			clanOffensiveWeight += weight.offensiveWeight;
			weights[i] = weight;
		}
		ClanWeight clanWeight = new ClanWeight();
		clanWeight.name = clan.name;
		clanWeight.tag = clan.tag;
		clanWeight.offensiveWeight = clanOffensiveWeight;
		clanWeight.playerWeights = weights;
		return clanWeight;
	}
	
	private static double getPlayerOffensiveWeight(PlayerProfile player)
	{
		double offensiveWeight = 0;
		
		ArrayList<Troop> offenses = new ArrayList<>();
		offenses.addAll(Arrays.asList(player.troops));
		offenses.addAll(Arrays.asList(player.spells));
		offenses.addAll(Arrays.asList(player.heroes));
		
		for(Troop offense : offenses)
		{
			if(WarWeights.WAR_WEIGHTS.containsKey(offense.name))
			{
				double[] weights = WarWeights.WAR_WEIGHTS.get(offense.name);
				if(weights.length > offense.level - 1)
				{
					double weight = weights[offense.level - 1];
					offensiveWeight += weight;
				}
				else
				{
					System.out.println("Level too high for " + offense.name + ". Level is " + offense.level);
				}
			}
			else
			{
				System.out.println("Could not find offense: " + offense.name);
			}
		}
		return offensiveWeight;
	}
}
