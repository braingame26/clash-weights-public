package com.evansalex.clashWarWeightsCmdLine;

import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;

import com.evansalex.clashWarWeightsCmdLine.pojo.Clan;
import com.evansalex.clashWarWeightsCmdLine.pojo.PlayerProfile;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ClashApi
{
	private ObjectMapper jsonMapper;
	
	public ClashApi()
	{
		jsonMapper = new ObjectMapper();
		jsonMapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
		jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	
	public Clan getClan(String clanTag)
	{
		clanTag = clanTag.replace("#", "%23");
		
		ClientResource resource = new ClientResource("http://evansalex.com/api/coc/v1/clans/" + clanTag);
		
		Representation response = resource.get(MediaType.APPLICATION_JSON);
		
		Clan clan = null;
		
		try
		{
			String txt = MakeValidJson(response.getText());
			clan = jsonMapper.readValue(txt, Clan.class);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		
		return clan;
	}
	
	public PlayerProfile getPlayerProfile(String playerTag)
	{
		playerTag = playerTag.replace("#", "%23");
		
		ClientResource resource = new ClientResource("http://evansalex.com/api/coc/v1/players/" + playerTag);
		
		Representation response = resource.get(MediaType.APPLICATION_JSON);
		
		PlayerProfile player = null;
		
		try
		{
			String txt = MakeValidJson(response.getText());
			player = jsonMapper.readValue(txt, PlayerProfile.class);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		
		return player;
	}
	
	private String MakeValidJson(String txt)
	{
		txt = txt.replaceAll("True", "true");
		txt = txt.replaceAll("False", "false");
		
		return txt;
	}
}
